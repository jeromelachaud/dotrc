## G's init

First, clone this repo :

```sh
git clone https://github.com/jeromelachaud/dotrc
cd dotrc
```

Then, edit the `config.json` file and run  :

```sh
make install
```
