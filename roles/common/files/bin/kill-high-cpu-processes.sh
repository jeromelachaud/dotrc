#!/bin/sh

PROCESS="firefox.*-childID|slack"

ps -eo pid,%cpu,cmd --sort=-%cpu | grep -E "$PROCESS" | awk '{if($2 > 50) {system("notify-send \"New kitten saved!\nkill "$1"\""); system("kill -9 "$1)}}'
