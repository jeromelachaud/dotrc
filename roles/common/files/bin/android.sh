#!/bin/sh

APP=$0

help() {
  echo "$APP mount | umount"
}

case "$1" in
  "mount")
    mkdir -p ~/tmp/android
    jmtpfs ~/tmp/android
    ;;
  "umount")
    fusermount -u ~/tmp/android
    ;;
  *)
    help
esac
