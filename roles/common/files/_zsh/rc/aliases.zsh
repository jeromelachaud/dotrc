## Aliases
# Zsh
alias srczsh="source ~/.zshrc"

# Git
alias git='LANG=en_US.UTF-8 git'

# NPM
alias ns="npm start"
alias ni="npm install"
alias nt="npm test"
alias nrw="npm run watch"
alias nrb="npm run build"
alias nrd="npm run dev"

# Yarn
alias yb="yarn run build"
alias yd="yarn run dev"
alias ys="yarn start"
alias yl="yarn link"
alias yt="yarn test"
alias yrw="yarn run watch"
alias yds="yarn dev:server"
alias ydc="yarn dev:client"

# Misc.
alias clr="clear"
alias wb="curl wttr.in/brive-la-gaillarde"
alias la="ls -lah"
alias ap="ansible-playbook -i HOSTS --ask-become-pass --extra-vars "@config.json" setup.yml"