ifeq (, $(shell which python))
  ifeq (, $(shell which python3))
    $(error "Please install python or python3")
  else
    ifeq (, $(shell which pip3))
      $(error "Please install python or python3")
    else
      PYTHON = $(shell which python3)
      PIP = $(shell which pip3)
      ANSIBLE = $(shell ${PYTHON} -m site --user-base)/bin/ansible-playbook
      MOLECULE = $(shell ${PYTHON} -m site --user-base)/bin/molecule
      export PATH := ${PATH}:$(shell ${PYTHON} -m site --user-base)/bin
    endif
  endif
else
  ifeq (, $(shell which pip))
    ifeq (, $(shell which python3))
      $(error "Please install python or python3")
    else
      ifeq (, $(shell which pip3))
        $(error "Please install python or python3")
      else
        PYTHON = $(shell which python3)
        PIP = $(shell which pip3)
        ANSIBLE = $(shell ${PYTHON} -m site --user-base)/bin/ansible-playbook
        MOLECULE = $(shell ${PYTHON} -m site --user-base)/bin/molecule
        export PATH := ${PATH}:$(shell ${PYTHON} -m site --user-base)/bin
      endif
    endif
  else
    PYTHON = $(shell which python)
    PIP = $(shell which pip)
    ANSIBLE = $(shell ${PYTHON} -m site --user-base)/bin/ansible-playbook
    MOLECULE = $(shell ${PYTHON} -m site --user-base)/bin/molecule
    export PATH := ${PATH}:$(shell ${PYTHON} -m site --user-base)/bin
  endif
endif

V ?= 0

verbose_0 = @
verbose_2 = set -x;
verbose = $(verbose_$(V))

ansible_verbose_1 = --verbose
ansible_verbose_2 = --verbose
ansible_verbose=$(ansible_verbose_$(V))

all: test

env:
  @echo $(PYTHON)
  @echo $(PIP)
  @echo $(ANSIBLE)
  @echo $(MOLECULE)
  @echo $(PATH)

test: install_test_deps ## Run ansible tests
  $(verbose) cd roles/common ; $(MOLECULE) test

install: install_deps ## Run installation
  $(verbose) $(ANSIBLE) -e 'ansible_python_interpreter=$(PYTHON)' $(ansible_verbose) setup.yml -i HOSTS --ask-become-pass --extra-vars "@config.json"

install_test_deps: install_deps
  $(verbose) $(PIP) install -r requirements-test.txt --user

install_deps: ${PYTHON} ${PIP}
  $(verbose) $(PIP) install -r requirements.txt --user

# Help
help: ## Show this help.
  $(verbose) echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[33m\1\\033[m:\2/' | column -c2 -t -s :)"
